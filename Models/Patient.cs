﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PatientRecordsDemo.Models
{
    public class Patient
    {
        // Nullable since new patient records won't have an ID yet generated
        public int? PatientID { get; set; } 
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }

        // This is declared as a string due to C# Date and SQL Dates sometimes not playing nice together
        public string DateOfBirth { get; set; }
    }
}
