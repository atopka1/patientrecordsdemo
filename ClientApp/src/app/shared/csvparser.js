const csv = require("csvtojson");

class CSVParser {

    static csvToJson(raw) {
        return csv({
            noheader: false,
            output: "json"
        })
        .fromString(raw)
        // .then((jsonObj) => {
        //     console.log("JSON", jsonObj)
        // })
    }
    
}

module.exports.CSVParser = CSVParser;