import { Injectable } from "@angular/core";
import { MessageService } from 'primeng/api';

// Abstraction of PrimeNG's messageService to more easily create toasts
@Injectable()
export class ToastService {

    constructor(private messageService: MessageService) {

    }

    private showToast(severity, title, message) {
        this.messageService.add({
            severity: severity
            ,summary: title
            ,detail: message
            ,life: 5000
        })
    }

    public success(title, message) {
        this.showToast('success', title, message);
    }

    public error(title, message) {
        this.showToast('error', title, message);
    }

}