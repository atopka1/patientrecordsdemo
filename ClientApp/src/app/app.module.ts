import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { PatientRecordsComponent } from './modules/patient-records/patient-records.component';
import { PatientRecordsService } from './modules/patient-records/patient-records.service';
import { PatientRecordsModule } from './modules/patient-records/patient-records.module';
import { MessageService } from 'primeng/api';
import { ToastService } from './shared/toast.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
  ],
  imports: [
    CommonModule,
    PatientRecordsModule,
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: PatientRecordsComponent, pathMatch: 'full' },
    ], { useHash: true })
  ],
  providers: [PatientRecordsService, MessageService, ToastService],
  bootstrap: [AppComponent]
})
export class AppModule { }
