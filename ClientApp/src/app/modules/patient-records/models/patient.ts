export class Patient {
    patientID?: number
    firstName: string
    lastName: string
    dateOfBirth?: Date
    gender?: string
}