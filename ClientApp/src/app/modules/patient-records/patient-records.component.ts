import { Component, OnInit } from '@angular/core';
import { PatientRecordsService } from './patient-records.service';
import { Patient } from "./models/patient";
import { CSVParser } from "../../shared/csvparser.js";
import { ToastService } from "../../shared/toast.service";
import { ConfirmationService } from 'primeng/api';
import { invalid } from '@angular/compiler/src/render3/view/util';
@Component({
    selector: "patient-records",
    templateUrl: "./patient-records.component.html"
})
export class PatientRecordsComponent implements OnInit {

    // Existing patient data
    patients: Patient[];

    // Used to open and close the sidebar
    showMapColumns: boolean = false;

    // The user will map columns from their CSV to this
    mappedColumns: any = {
        firstName: null,
        lastName: null,
        dateOfBirth: null,
        gender: null
    }

    // These columns will have to match what is accepted in the request body of the API
    mappableColumns: MappableColumn[] = [
        {
            name: "firstName",
            label: "First Name"
        },
        {
            name: "lastName", 
            label: "Last Name"
        },
        {
            name: "dateOfBirth",
            label: "Date of Birth"
        },
        {
            name: "gender"
            ,label: "Gender"
        }
    ]

    // These will be the columns in the CSV that the user uploaded
    userColumns: string[] = [];

    // CSV data converted to JSON, we won't know the keys here yet
    uploadedData: any[];

    constructor(private api: PatientRecordsService, private toastService: ToastService, private confirmationService: ConfirmationService) {

    }

    ngOnInit(): void {
        this.getAllPatients();
    }

    // Retrieve all existing patient data
    getAllPatients(): void {
        this.api.getAllPatients().subscribe((data: Patient[]) => this.patients = data)
    }
 
    // Called when file is selected
    onUpload(event): void {
        // Check for empty file or invalid file
        if (!event.target.files || event.target.files.length == 0) {
            this.toastService.error("Error", "No file uploaded");
            return;
        }
        try {
            const rawFile: File = event.target.files[0];
            if (!this.verifyFileType(rawFile.name)) {
                this.toastService.error("Error", "Please upload a .csv file");
                return;
            }

            const reader: FileReader = new FileReader();
            reader.readAsText(rawFile);
    
            reader.onload = () => {
                // Use a custom csv parser library to convert the string CSV data to JSON
                const csvData: any = reader.result;
                CSVParser.csvToJson(csvData)
    
                    .then((jsonObj) => {
                        // Validate that there's data in the file
                        if (!jsonObj || jsonObj.length == 0) {
                            this.toastService.error("Error", "No data found in uploaded file");
                            return;
                        }
    
                        // Now that we have a raw JSON object, we want the user to manually select which columns to map to which fields
                        // This provides flexibility to the layout of the CSV files
                        
                        // First take the keys of the input CSV
                        // These will be used to fill our dropdowns for mapping
                        const keys: string[] = Object.keys(jsonObj[0])
                        this.userColumns = keys;
    
                        // We need to initialize the mapped columns to just be the first available, otherwise it will stay as null if the user doesn't change the selected column
                        for (let col in this.mappedColumns) {
                            this.mappedColumns[col] = this.userColumns[0];
                        }
    
                        // Store the uploaded JSON data
                        this.uploadedData = jsonObj;
    
                        // Finally show the sidebar to allow user to map columns
                        this.showMapColumns = true;
                    });
            };
        }
        catch (ex) {
            this.toastService.error("Error", "Could not read CSV file. Please review uploaded file and try again");
        }

    }

    verifyFileType(name: string): boolean {
        // Verify the extension is .csv
        return name.split(".").pop() === "csv";
    }

    // Called when 'Submit' is clicked within the sidebar
    submit(): void {
        const requestBody: Patient[] = this.buildRequestBody();

        // Uncomment to debug or peek at how everything looks before continuing
        //console.log("mappedColumns", this.mappedColumns);
        //console.log("uploadedData", this.uploadedData);
        //console.log("REQUEST BODY!", requestBody);

        // Now that we have all the data formatted for submission, we need to VALIDATE 
        // "Future" enhancement => prompt the user to overwrite existing records (currently overwrite automatically for same FirstName, LastName, and DateOfBirth)
        if (this.validate(requestBody)) {
            this.api.uploadPatients(requestBody).subscribe(
                (data: any) => {
                    const successfulRecordCount = data.numSuccess;
                    this.toastService.success("Success", `${successfulRecordCount} records uploaded`);

                    const invalidRecordCount = data.errors.count;
                    if (invalidRecordCount > 0) {
                        this.toastService.error("Error", `${invalidRecordCount} records were invalid and were not uploaded`);
                    }

                    // Refresh patient data
                    this.getAllPatients();

                    // Close the sidebar
                    this.showMapColumns = false;
                },
                (error) => {
                    this.toastService.error("Error", "There was an error uploading records. We apologize for the inconvenience");
                }
            )
        }
        else {
            const message = "One or more records is missing First Name, Last Name, or Date of Birth. Please correct the imported file and try again.";
            this.toastService.error("Error", message);
        }
        
    }

    // Using all the mapping objects, construct the request body to ultimately send to API
    // Ideally this should check that the same column was not mapped twice but I did not implement that here
    buildRequestBody(): Patient[] {
        const requestBody: Patient[] = [];
        this.uploadedData.forEach(row => {

            const patient: Patient = {
                firstName: row[this.mappedColumns.firstName]
                , lastName: row[this.mappedColumns.lastName]
                , dateOfBirth: row[this.mappedColumns.dateOfBirth]
                , gender: row[this.mappedColumns.gender]
            }
            requestBody.push(patient);
        })  
        return requestBody;
    }

    // I am using the assumption that same FirstName, LastName, and DateOfBirth together should overwrite existing records (or prompt user to do so)
    // Therefore, if any of these records have any of those fields missing, it needs to reject the file
    validate(data: Patient[]): boolean {
        return data.every(patient => (patient.firstName && patient.lastName && patient.dateOfBirth))
    }

    // This is mostly for testing purposes to I can clear records instead of manually emptying the SQL tables
    clearRecords(): void {
        this.confirmationService.confirm({
            message: "This will delete all records and cannot be undone. Continue?"
            ,accept: () => {
                this.api.clearPatientData().subscribe(() => {
                    this.toastService.success("Success", "Patient records deleted");

                    // Refresh patient data
                    this.getAllPatients();
                })
            }
        })
    } 

    onRowEditSave(patient: Patient): void {
        this.api.updatePatient(patient).subscribe(
            (data) => {
                this.toastService.success("Success", "Patient updated");
            },
            (error) => {
                this.toastService.error("Error", "Could not update patient. Please ensure all entered fields are valid.");

                // Kinda hacky fix: refresh the data on error so that entered fields don't look like they saved
                // This is due to me using the patient data as the ngModel for editing, rather than cloning the data and using a separate datasource for edit
                this.getAllPatients();
            }
        )
    }


}

interface MappableColumn {
    // Key to reference mapping object
    name: string

    // Label to show to user
    label: string
}