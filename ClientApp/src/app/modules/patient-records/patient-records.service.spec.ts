// NOTE: This was my attempt at a unit test for the PatientServices
// However, I ran into an issue with "R3InjectorError" even though the service is listed as a provider for the module
// I didn't want to spend too much time trying to implement the unit testing so I left it here, but it errors when run with 'ng test'

import { HttpClient, HttpHandler } from "@angular/common/http";
import { TestBed, inject } from "@angular/core/testing";
import { PatientRecordsService } from "./patient-records.service";
import { Patient } from "./models/patient";
import {
    HttpClientTestingModule,
    HttpTestingController
} from "@angular/common/http/testing";


describe("PatientRecordService", () => {

    let patientRecordService: PatientRecordsService;
    let httpTestingController: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule]
        });
        httpTestingController = TestBed.get(HttpTestingController);
    });
    beforeEach(inject(
        [PatientRecordsService],
        (service: PatientRecordsService) => {
            patientRecordService = service;
        }
    ));

    it("should return patient data", () => {
        let result: Patient[];
        const url = "/api/patients";
        patientRecordService.getAllPatients().subscribe(d => {
            result = d;
        })
        const req = httpTestingController.expectOne({
            method: "GET",
            url: url
        });

        // I am unsure how to proceed here, the examples I am using compare the result of this endpoint to a specific record
        // However, I will not know which specific record(s) will be returned by this endpoint, since that data can change

        // Another example recommends just checking the type
        expect(result instanceof Patient).toBe(true, 'instance of Patient');
    })
})