import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from "@angular/core";
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { SidebarModule } from 'primeng/sidebar';
import { DropdownModule } from 'primeng/dropdown';
import { ToastModule } from 'primeng/toast';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';

import { PatientRecordsComponent } from "./patient-records.component";
import { PatientRecordsService } from "./patient-records.service";
@NgModule({
    declarations: [
        PatientRecordsComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        CommonModule,
        TableModule,
        ButtonModule,
        InputTextModule,
        SidebarModule,
        DropdownModule,
        ToastModule,
        ConfirmDialogModule
    ],
    exports: [
        PatientRecordsComponent
    ],
    providers: [
        PatientRecordsService,
        ConfirmationService
    ]
})
export class PatientRecordsModule { }
