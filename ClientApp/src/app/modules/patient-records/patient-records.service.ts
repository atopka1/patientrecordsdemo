import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Patient } from "./models/patient";

@Injectable() 
export class PatientRecordsService {
    private headers;

    constructor(private http: HttpClient) {
        const headers: Headers = new Headers();
        headers.append("Content-Type", "application/json");
        this.headers = { headers: headers }
    }

    // Retrieve all patient data
    public getAllPatients() {
        const endpoint = "/api/patients";
        return this.http.get<Patient[]>(endpoint);
    }

    // Upload an array of patients
    public uploadPatients(data: Patient[]) {
        const endpoint = "/api/patients";
        return this.http.post(endpoint, data, this.headers);
    }

    // Delete all patient data
    public clearPatientData() {
        const endpoint = "/api/patients";
        return this.http.delete(endpoint);
    }

    // Delete a patient by ID
    public deletePatient(patientID: number) {
        const endpoint = `/api/patients/${patientID}`;
        return this.http.delete(endpoint);
    }

    // Update a patient by ID
    public updatePatient(patient: Patient) {
        const endpoint = `/api/patients/${patient.patientID}`;
        return this.http.patch(endpoint, patient, this.headers);
    }


}