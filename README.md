# Patient Record Demo
Author: Amy Topka

Challenge courtesy of Rethink First. This demo allows sample patient records to be uploaded, viewed, and edited.

This application uses a postgreSQL database hosted on Heroku.

## Installation
Navigate to ClientApp/ and use npm to install required packages

```bash
npm install
```

## REST API
The following endpoints are included in this project

### GET /patients
Retrieves all patient data

### POST /patients
Upload an array of patient data and saves to postgreSQL. Any records uploaded with duplicate First Name, Last Name, and Date of Birth will be overwritten.

### PATCH /patients/{id}
Update one or more fields for a patient by ID

### DELETE /patients/{id}
Delete a patient by id

### DELETE /patients
Clear all patient data