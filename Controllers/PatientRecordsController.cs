﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using PatientRecordsDemo.Models;
using PatientRecordsDemo.Helpers;
using Newtonsoft.Json;

namespace PatientRecordsDemo.Controllers
{
    [ApiController]
    [Route("api")]
    public class PatientRecordsController : Controller
    {
        [HttpGet]
        [Route("patients")]
        public IActionResult GetAllPatients()
        {
            try
            {
                PatientRecordsSQLHandler handler = new PatientRecordsSQLHandler();
                List<Patient> result = handler.GetPatientRecords();
                return Ok(result);
            }
            catch
            {
                // throwing a new exception here to mask any SQL information in the response (in the case a 3rd-party would call this)
                // not really sure what's the best practice here, however
                // In Startup.cs this is configured to return alongside a 500 status code
                throw new Exception("Could not retrieve patient data");
            }
        }

        [HttpPost]
        [Route("patients")]
        public IActionResult CreatePatients([FromBody] List<Patient> patients)
        {
            // This merges patient data, overwriting existing records on the basis of same first name, last name, and date of birth
            // Ideally this should have been split into two endpoints, one to create which rejects any duplicates, another to update
            // Then the user on the front-end would be able to confirm overwrite of existing records
            // But, for the sake of time I kept this as one "merge" endpoint with a separate "update" that just patches individual fields
            try
            {
                PatientRecordsSQLHandler handler = new PatientRecordsSQLHandler();

                // Track patients that errored
                List<Patient> erroredPatients = new List<Patient>();

                // Track succesful uploads
                int numSuccess = 0;

                // Iterate through the input patient data
                // As a "future" enhancement this should become a bulk process in the case of uploading many records
                // But for now let's do one-by-one
                foreach(Patient patient in patients)
                {
                    // Try/Catch individually so we can track which records failed
                    try
                    {
                        handler.CreatePatientRecord(patient);
                        numSuccess++;
                    }
                    catch (Exception e)
                    {
                        // Could expand this to show reason for each errored patient
                        erroredPatients.Add(patient);
                    }
                };

                // Create a response so we see { "Errors" : [ <list of patients not created> ] }
                // And "numSuccess" : <number>
                Dictionary<string, object> response = new Dictionary<string, object>();
                response.Add("errors", erroredPatients);
                response.Add("numSuccess", numSuccess);

                return Ok(response);
            }
            catch
            {
                throw new Exception("Could not create patient data");
            }
        }

        [HttpPatch]
        [Route("patients/{id}")]
        public IActionResult UpdatePatient([FromBody] Patient patient)
        {
            try
            {
                PatientRecordsSQLHandler handler = new PatientRecordsSQLHandler();
                handler.UpdatePatient(patient);
                return Ok();
            }
            catch
            {
                throw new Exception("Could not update patient");
            }
        }

        [HttpDelete]
        [Route("patients/{id}")]
        public IActionResult DeletePatient(int id)
        {
            // Not actually implemented on the front-end
            try
            {
                PatientRecordsSQLHandler handler = new PatientRecordsSQLHandler();
                handler.DeletePatient(id);

                return Ok();
            }
            catch
            {
                throw new Exception("Could not delete patient");
            }
        }

        [HttpDelete]
        [Route("patients")]
        public IActionResult ClearPatientData()
        {
            try
            {
                PatientRecordsSQLHandler handler = new PatientRecordsSQLHandler();
                handler.ClearAllPatientRecords();

                return Ok();
            }
            catch
            {
                throw new Exception("Could not clear patient data");
            }
        }
    }
}
