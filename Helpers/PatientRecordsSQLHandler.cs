﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using PatientRecordsDemo.Models;

namespace PatientRecordsDemo.Helpers
{
    // SQL queries for patient records
    public class PatientRecordsSQLHandler : PostgreSQLHandler
    {
        public PatientRecordsSQLHandler()
        {

        }

        // Create a single patient record
        public void CreatePatientRecord(Patient patient)
        {
            // 'public' is the name of the PostgreSQL schema where my table is
            string query = @"INSERT INTO public.Patients (firstName, lastName, dateOfBirth, gender) 
                            VALUES (@firstName, @lastName, @dateOfBirth, @gender)
                            ON CONFLICT(firstName, lastName, dateOfBirth)
                            DO UPDATE 
                                SET gender = @gender
                            ";

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("firstName", patient.FirstName);
            parameters.Add("lastName", patient.LastName);
            parameters.Add("dateOfBirth", Convert.ToDateTime(patient.DateOfBirth));

            // I HATE how annoying it is to get SQL and C# to play well with null values
            // regular null not accepted here, but DBNull.Value causes issues when selecting that record
            // Coalescing operator also doesn't work here?
            //parameters.Add("gender", patient.Gender ?? DBNull.Value);
            parameters.Add("gender", patient.Gender == null ? DBNull.Value : patient.Gender);


            ExecuteQuery(query, parameters);
        }

        // Get all patient records
        public List<Patient> GetPatientRecords()
        {
            // Each column needs to be aliased to match case of the C# Model
            // PostgreSQL by default converts everything to lower case
            string query = @"SELECT    patientID AS ""PatientID""
                                        ,firstName AS ""FirstName""
                                        ,lastName AS ""LastName""
                                        ,gender AS ""Gender""
                                        ,TO_CHAR(dateOfBirth, 'YYYY-MM-DD') AS ""DateOfBirth""
                            FROM public.Patients";
            return ExecuteQueryToList<Patient>(query);
        }

        public void ClearAllPatientRecords()
        {
            string query = "DELETE FROM public.Patients";
            ExecuteQuery(query);
        }

        public void DeletePatient(int patientID)
        {
            string query = @"DELETE FROM public.Patients WHERE patientID = @patientID";
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("@patientID", patientID);

            ExecuteQuery(query);
        }

        public void UpdatePatient(Patient patient)
        {
            // I never know how to properly patch individual fields. The drawback of this is if you want to intentionally null out a field, you cannot.
            string query = @"UPDATE public.Patients 
                            SET firstName = COALESCE(@firstName, firstName)
                                ,lastName = COALESCE(@lastName, lastName)
                                ,dateOfBirth = COALESCE(@dateOfBirth, dateOfBirth)
                                ,gender = COALESCE(@gender, gender)
                            WHERE patientID = @patientID";
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("@firstName", patient.FirstName == null ? DBNull.Value : patient.FirstName);
            parameters.Add("@lastName", patient.LastName == null ? DBNull.Value : patient.LastName);
            parameters.Add("@dateOfBirth", patient.DateOfBirth == null ? DBNull.Value : Convert.ToDateTime(patient.DateOfBirth));
            parameters.Add("@gender", patient.Gender == null ? DBNull.Value : patient.Gender);
            parameters.Add("@patientID", patient.Gender == null ? DBNull.Value : patient.PatientID);


            ExecuteQuery(query, parameters);
        }
    }
}
