﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace PatientRecordsDemo.Helpers
{
    // This will handle all connections to PostgreSQL and can be inherited to create individual SQL adapters for different features
    // I have this hosted on Heroku to avoid being chained to using PostgreSQL locally
    public abstract class PostgreSQLHandler
    {
        public PostgreSQLHandler()
        {

        }

        // Retrieve string for connection to PostgreSQL
        protected string GetConnectionString()
        {
            // Bad to store db information in-code, normally I would use something like Microsoft Azure's Key Vault to manage these credentials
            string host = "ec2-44-198-204-136.compute-1.amazonaws.com";
            string username = "bdxjqkrxtycroc";
            string password = "2a7d5908c71295b84d5a48c247e739944e436653d3085be7423a20e5a3d0f2a5";
            string database = "dk7ph7o3mq6ot";

            return $"Host={host};Username={username};Password={password};Database={database};sslmode=Require;Trust Server Certificate=true";
            //return "postgres://bdxjqkrxtycroc:2a7d5908c71295b84d5a48c247e739944e436653d3085be7423a20e5a3d0f2a5@ec2-44-198-204-136.compute-1.amazonaws.com:5432/dk7ph7o3mq6ot";
        }

        // Execute a query that does not return any records
        // query - the query to execute, using parameter placeholders instead of using the raw parameter values
        // parameters (optional) - a dictionary of parameters to inject into the query. Key: parameter name, Value: parameter value.
        // e.g. ('firstName', 'Shlomi')
        protected void ExecuteQuery(string query, Dictionary<string, object> parameters = null)
        {
            string connectionString = GetConnectionString();
            using NpgsqlConnection conn = new NpgsqlConnection(connectionString);
            conn.Open();

            // Use NpgsqlCommand to fill out parameters instead of storing the entire query (with its raw parameters) as a string. This helps to avoid SQL injection from user-provided inputs.
            try
            {
                using (NpgsqlCommand cmd = new NpgsqlCommand(query, conn))
                {
                    if (parameters != null && parameters.Count > 0)
                    {
                        foreach (KeyValuePair<string, object> parameter in parameters)
                        {
                            cmd.Parameters.AddWithValue(parameter.Key, parameter.Value);
                        }
                    }
                    cmd.ExecuteNonQuery();
                }
            }
            catch (NpgsqlException e)
            {
                throw;
            }
            finally
            {
                conn.Close();
            }
        }



        // Same as above but returns the results of the query as a List<T>
        // query - the query to execute, using parameter placeholders instead of using the raw parameter values
        // parameters (optional) - a dictionary of parameters to inject into the query. Key: parameter name, Value: parameter value.
        // e.g. ('firstName', 'Shlomi')

        protected List<T> ExecuteQueryToList<T>(string query, Dictionary<string, object> parameters = null)
        {

            string connectionString = GetConnectionString();
            using NpgsqlConnection conn = new NpgsqlConnection(connectionString);
            conn.Open();

            try
            {
                using (NpgsqlCommand cmd = new NpgsqlCommand(query, conn))
                {
                    if (parameters != null && parameters.Count > 0)
                    {
                        foreach (KeyValuePair<string, object> parameter in parameters)
                        {
                            cmd.Parameters.AddWithValue(parameter.Key, parameter.Value);
                        }
                    }

                    // There is no easy way to generically use the NpgDataReader to convert the SQL results to a list
                    // So here I fill a datatable and convert that to a list manually
                    // This should automatically preserver all data types without having to explicitly tell a DataReader what to grab
                    NpgsqlDataAdapter dataAdapter = new NpgsqlDataAdapter(cmd);
                    DataSet dataSet = new DataSet();
                    dataAdapter.Fill(dataSet);

                    // Verify here that there exists tables in the dataSet before returning the one at index 0
                    // This could be expanded upon if you wanted to return multiple results within the same query (not sure if supported by PostgreSQL)
                    if (dataSet.Tables.Count > 0)
                    {
                        DataTable raw = dataSet.Tables[0];
                        // Use my "stolen" method to convert this to a List
                        return DataTableToList.ConvertDataTableToList<T>(raw);
                    }
                    // Otherwise throw exception because there should have been at least a blank DataTable if the query actually had a return in it
                    else
                    {
                        throw new Exception("No data was returned by query");
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
